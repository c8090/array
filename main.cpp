#include <iostream>

/*  $This program creates an array of size index and has you input the objects you would like to store and search
 *      through; then asks you for an object to search for inside the array you created and displays back to you
 *      the index of where the word you searched for is at and also the objects searched through; This runs on a
 *      loop until you tell them program you no longer want to search anymore.
 *
 *
 * start
 *  Declarations:
 *  int index
 *  string search_word
 *  bool search_on
 *  bool found
 *  string answer
 *
 *  search_on = true
 *  while (search_on)
 *      found = false
 *      output "Enter the size of index you would like to search"
 *      input index
 *      declare: string arr[index]
 *      output: "Enter " + index + " objects to search:"
 *      for (int i; i < index; i++)
 *          input: arr[i]
 *      endfor
 *      output: "enter the object to search for: "
 *      input: search_word
 *      for (int i = 0; i < index; i++)
 *          if (arr[i] ==k)
 *              output: "Word " + search_word + "found at index: #" + i
 *              found = true
 *          endif
 *      endfor
 *      if (!found)
 *          output "Seached objects: '";
 *          for (int i = 0; i < index; i++)
 *              output: arr[i] + " "
 *          endfor
 *          output: "' \nWord" + search_word + " was not found."
 *      else
 *          output: "Searched objects: '"
 *          for (int i = 0; i < index; i++)
 *               output: arr[i] + " "
 *          endfor
 *          output: "'"
 *      endif
 *      output: "Enter new search"
 *      input: answer
 *      if (answer == "no")
 *          search_on = false
 *      endif
 *  endwhile
 *  output: "Thanks for searching. Have a great day"
 *  endprogram
 *
 */
using namespace std;

int main() {
    int index;
    string search_word;
    bool search_on;
    bool found;
    string answer;


    search_on = true;
    while (search_on) {
        found = false;
        cout << "Enter size of array" << endl;
        cin >> index;
        string array[index];
        cout << "Enter " << index << " objects to search\n...";
        for (int i = 0; i < index; i++) {
            cin >> array[i];
        }
        cout << "Object to search for?" << endl;
        cin >> search_word;
        for (int i = 0; i < index; i++) {
            if (array[i] == search_word) {
                cout << "Word '" << search_word << "' found at index: #..." << i << endl;
                found = true;
            }


        }
        if (!found) {
            cout << "Searched objects: '";
            for (int i = 0; i < index; i++) {
                cout << array[i] << " ";
            }
            cout << "' \nWord '" << search_word << "' not found." << endl;
        } else {
            cout << "Searched objects: '";
            for (int i = 0; i < index; i++) {
            cout << array[i] << "--";
            }
            cout << "'" << endl;
        }
        cout << "Enter a new search?";
        cin >> answer;
        if (answer == "no" || answer == "No" || answer == "NO") search_on = false;
    }
    cout << "Thanks for searching! Have a great day!" << endl;
    return 0;
}
